using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeUI : MonoBehaviour
{
   private Node target;
   public GameObject canvasUI;

   public void SetTarget(Node _target){
       //should set the target
       target = _target;
      //move the UI in the position of the node
       transform.position = target.GetBuildPosition();
       canvasUI.SetActive(true);
   }

   public void Hide(){
       //hides upgrade UI when not in use
      canvasUI.SetActive(false);
   }
}
