using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{

    private Transform target;
    [Header("General")]
    public float range = 15f;
    [Header("Weapons")]
    public float fireRate = 1f;
    public GameObject  bulletPrefab;
    [Header("Queen Sorcerer beam")]
    public bool usemagicBeam = false;
    public LineRenderer lineRenderer;
    private float fireCountdown = 0f;
    [Header("Unity required fields")]
    public string enemyTag = "Enemy";
    public Transform rotateCannon;
    public Transform firePoint;
    public float turnSpeed = 10f;
    private void Start() 
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        //return the distance in units and stored in a float
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if(distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if(nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }else{
            target = null;
        }
    }

    private void Update() 
    {
        if(target == null)
        {
            return;
        }
       LockOnTarget();

       if(usemagicBeam){
          MagicBeam();
       }else{
       // Shoots out the prefab

        if(fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f/fireRate;
        }

        fireCountdown -= Time.deltaTime;
       }
    }

    public void MagicBeam(){
        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, target.position);
    }

    public void LockOnTarget(){
         //Rotates the cannon in the direction of the enemies
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(rotateCannon.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        rotateCannon.rotation = Quaternion.Euler(0f,rotation.y, 0f);
    }

    private void Shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if(bullet != null)
        {
            //if w=doesnt work change target to bullet target
            bullet.Follow(target);
        }
    }

    //draws a sphere range for targets
    
    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    private void CheckForEnemies(){
        Collider[] colliders = Physics.OverlapSphere(transform.position, 20f);

        foreach(Collider collider in colliders){
            if(collider.GetComponent<EnemyMovement>()){
                collider.GetComponent<EnemyMovement>().PathEnded();
            }
        }
    }
}
