using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour
{
   public TextMeshProUGUI roundsText;

   private void OnEnable() {
       //will display the rounds text and update the rounds survived
       roundsText.text = PlayerStats.Rounds.ToString();
   }

   public void Retry(){
       SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
   }

   public void MainMenu(){
       SceneManager.LoadScene(0);
   }
}
