using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public Color hoverColor;
    public Color notEnoughMoneyColor;
    private Renderer rend;
    public Vector3 positionOffset;
    private Color startColor;
    [Header("Optional")]
    public GameObject cannon;
    BuildManager buildManager;

    private void Start() 
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildManager = BuildManager.instance;
    }

    public Vector3 GetBuildPosition(){
        return transform.position + positionOffset;
    }

    private void OnMouseDown() {

        if(cannon != null){
            buildManager.SelectNode(this);
            return;
        }
        //checks if player can build
        if(!buildManager.CanBuild)
        return;
        //build a Cannon on this node
        buildManager.BuildCannonOn(this);
    }

    
  private void OnMouseEnter()
  {
      if(!buildManager.CanBuild)
        return;
      
      if(buildManager.HasMoney){
      rend.material.color = hoverColor;
       
      }else{
          rend.material.color = notEnoughMoneyColor;
      }
  }

  private void OnMouseExit() 
  {
      rend.material.color = startColor;
  }
}
