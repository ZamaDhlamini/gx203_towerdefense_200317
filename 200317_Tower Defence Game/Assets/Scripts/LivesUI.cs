using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LivesUI : MonoBehaviour
{
    public TextMeshProUGUI livesTMP;

    private void Update() {
        //in a string
        livesTMP.text = PlayerStats.Lives + "LIVES";
    }
}
