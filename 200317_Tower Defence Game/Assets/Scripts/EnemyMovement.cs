
using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float speed = 10f;
    private Transform target;
    private int waypointIndex = 0;
    public int value = 50;
    public GameObject deathEffect;
    public int enemyHealth = 100;

    private void Start() {
        target = Waypoints.wayPoints[0];
    }

    public void TakeDamage(int amount){
        enemyHealth -= amount;

        if(enemyHealth <= 0){
            Die();
        }
    }

    private void Die(){
        PlayerStats.Money += value;

        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);
        
        Destroy(gameObject);
    }

    private void Update() 
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
    }

    private void GetNextWaypoint()
    {
        //enemy has reached last node
        if(waypointIndex >= Waypoints.wayPoints.Length - 1)
        {
            PathEnded();
            return;
        }
        waypointIndex++;
        target = Waypoints.wayPoints[waypointIndex];
    }

    public void PathEnded()
    {
        if(PlayerStats.Lives > 0){
        PlayerStats.Lives--;
        }
       Destroy(gameObject);
    }
}
