
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
   private Transform bulletTarget;
   public float speed = 70f;
   public int damage = 50;
   public float explosionRadius = 0f;
   
   public GameObject impactEffect;
   

   public void Follow(Transform target)
   {
       bulletTarget = target;
   }

    // Update is called once per frame
    void Update()
    {
        if(bulletTarget == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = bulletTarget.position - transform.position;
         float distanceInFrame = speed * Time.deltaTime;

         if(dir.magnitude <= distanceInFrame)
         {
             HitTarget();
             return;
         }

         transform.Translate (dir.normalized * distanceInFrame, Space.World);
         transform.LookAt(bulletTarget);
    }

    void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);

        if(explosionRadius > 0f)
        {
           Explode();
        }else
        {
           Damage(bulletTarget);
        }

        Destroy(gameObject);
        

    }

    void Explode(){
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach(Collider collider in colliders)
        {
            if(collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }

    void Damage(Transform enemy){
        EnemyMovement e = enemy.GetComponent<EnemyMovement>();

        if(e != null)
        {
        e.TakeDamage(damage);
        }
        Destroy(enemy.gameObject);
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }

    
}
