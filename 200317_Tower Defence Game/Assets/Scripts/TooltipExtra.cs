using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipExtra : MonoBehaviour
{
    public string message;

    private void OnMouseEnter() {
        Tooltip._instance.SetAndShowToolTip(message);
    }

    private void OnMouseExit() {
        Tooltip._instance.HideTooltip();
    }
}
