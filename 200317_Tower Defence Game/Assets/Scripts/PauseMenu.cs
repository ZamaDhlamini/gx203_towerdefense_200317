using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseUI;
    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)){
            Toggle();
        }
    }

    public void Toggle(){
        pauseUI.SetActive(!pauseUI.activeSelf);

        if(pauseUI.activeSelf){
            Time.timeScale = 0f;
        }else{
            Time.timeScale = 1f;
        }
    }

    

    
}
