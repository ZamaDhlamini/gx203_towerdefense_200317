using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;
    private CannonBlueprint cannonToBuild;
    private Node selectedNode;
    public GameObject cannonPrefab;
    public GameObject spearLauncherPrefab;
    public GameObject buildEffect;
    public UpgradeUI upgradeUI;
    //property to get and check if cannon to build is null
    public bool CanBuild { get { return cannonToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= cannonToBuild.cost; } }

   

    private void Awake() 
    {
        if(instance != null){
            Debug.LogError("More than one BuildManager in scene!");
        }
        instance = this;
    }

    public void BuildCannonOn(Node node){
        if(PlayerStats.Money < cannonToBuild.cost){
            Debug.Log("Not enough money!");
            return;
        }

        PlayerStats.Money -= cannonToBuild.cost;
        //adds the cannon to the node when wanting to build
        GameObject _cannon = (GameObject)Instantiate(cannonToBuild.prefab, node.transform.position, Quaternion.identity);
        node.cannon = _cannon;
        //adds the build partical effect to the cannon when put on a node
        GameObject effect = (GameObject)Instantiate(buildEffect, node.GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        Debug.Log("Cannon build! Money spent:" + PlayerStats.Money);
    }


    public void SelectNode(Node node){
        if(selectedNode == node){
            DeselectNode();
            return;
        }
        //setting selected node to the node I am passing
        selectedNode = node;
        cannonToBuild = null;

        upgradeUI.SetTarget(node);
    }

    public void DeselectNode(){
       selectedNode = null;
       upgradeUI.Hide();
    }


    

   public void SelectCannonToBuild(CannonBlueprint cannon){
        cannonToBuild = cannon;
     DeselectNode();
   }
}
