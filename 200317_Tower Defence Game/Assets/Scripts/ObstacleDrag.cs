using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDrag : MonoBehaviour
{
   private bool dragging = false;
    private float distance;
    private Vector3 startDist;

//destroys the enemy when collided with trigger
    private void OnTriggerEnter(Collider collider) {
        Destroy(gameObject);
    }

    

    private void OnMouseDown() {
        distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        dragging = true;
        //casting the ray to the screen of the camera onto the input mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 rayPoint = ray.GetPoint(distance);
        startDist = transform.position - rayPoint;
    }

    private void OnMouseUp() {
        dragging = false;
    }

    private void Update() {
         if (dragging)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);
            //Now we are adding the position of the raypoint to the start distance
            transform.position = rayPoint + startDist;
        }
    }
}
