using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowerManager : MonoBehaviour
{
    public static MoveTowerManager instance;
    [SerializeField] LayerMask layerMask;
    public bool isMoving;
    public GameObject chosenTower;

    private void Awake() {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if(isMoving && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 50, layerMask)){
            chosenTower.transform.position = hit.point;
        }
    }
}
