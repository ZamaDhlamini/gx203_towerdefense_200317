using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTooltip : MonoBehaviour
{
    
   

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if(Physics.Raycast(ray, out hit)){
           if(hit.collider.gameObject.name == "Obstacle"){
               //show Tooltip
               Debug.Log("Drag obstacle");
           }
        }
    }
}
