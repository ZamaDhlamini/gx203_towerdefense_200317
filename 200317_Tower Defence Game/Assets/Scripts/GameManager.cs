using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static bool gameIsOver = false;
    public GameObject gameOverUI;

    private void Start() {
        //start of game, there is no gameover
        gameIsOver = false;
    }
    
    //keeps track of when lives are finished 
    void Update()
    {
        if(gameIsOver)
        return;

        if(PlayerStats.Lives <= 0){
            EndGame();
        }
    }

    void EndGame(){
        //When lives are done then the game is over
        gameIsOver = true;
        gameOverUI.SetActive(true);
    }
}
