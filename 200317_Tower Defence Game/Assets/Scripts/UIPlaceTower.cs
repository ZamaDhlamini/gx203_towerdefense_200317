using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlaceTower : MonoBehaviour
{
    [SerializeField] GameObject queenTowerToSpawn;
    
    public void SpawnTower(){
        MoveTowerManager.instance.chosenTower = Instantiate(queenTowerToSpawn);
        MoveTowerManager.instance.isMoving = true;
    }
}
