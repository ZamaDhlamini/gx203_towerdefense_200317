using TMPro;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class WaveSpawner : MonoBehaviour
{
    public Transform enemyPrefab;
    public Transform enemyspawnPoint;
    public TextMeshProUGUI countdownWaveText;
    public float WaveTimer = 2f;
    private float countdown = 2f;
    private int waveNumber = 0;
    
    private void Start() {
        StartCoroutine(WaveSpawn());
    }
    

    private void Update() 
    {
        // if(countdown <= 0f)
        // {
        //     StartCoroutine(WaveSpawn());
        //     countdown = WaveTimer;
        // }

        // countdown -= Time.deltaTime;

        // countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        // countdownWaveText.text = string.Format("{0:00.00}", countdown);
    }

    IEnumerator WaveSpawn(){
        while (true)
        {
            
            yield return new WaitForSeconds(WaveTimer);
        for (int i = 0; i < waveNumber; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.5f);
        }

        waveNumber++;
        PlayerStats.Rounds++;
        //Debug.Log("Enemy Wave has started!");
        }
    }

    public void SpawnEnemy()
    {
        if(enemyPrefab != null){
        Instantiate(enemyPrefab, enemyspawnPoint.position, enemyspawnPoint.rotation);
        }
    }
}
