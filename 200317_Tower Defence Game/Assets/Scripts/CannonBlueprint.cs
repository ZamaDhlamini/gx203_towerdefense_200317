using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CannonBlueprint 
{
    public GameObject prefab;
    public int cost;
}
