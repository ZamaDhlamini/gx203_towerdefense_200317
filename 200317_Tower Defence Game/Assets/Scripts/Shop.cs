using UnityEngine;

public class Shop : MonoBehaviour
{
    public CannonBlueprint standardCannon;
    public CannonBlueprint spearLauncher;
    public CannonBlueprint queenSorcerer;
    BuildManager buildManager;

    private void Start() {
        buildManager = BuildManager.instance;
    }
    public void SelectStandardRookCannon(){
        Debug.Log("Cannon selected and placed");
        buildManager.SelectCannonToBuild(standardCannon);
    }

    public void SelectSpearLauncher(){
        Debug.Log("Another Spear Launcher selected and placed");
        buildManager.SelectCannonToBuild(spearLauncher);
    }
    public void SelectQueenSorcerer(){
        Debug.Log("Another Queen Sorcerer selected");
        buildManager.SelectCannonToBuild(queenSorcerer);
    }
}
