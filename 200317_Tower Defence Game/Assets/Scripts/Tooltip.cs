using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Tooltip : MonoBehaviour
{

    public static Tooltip _instance;
    public TextMeshProUGUI tooltipText;

    private void Awake() {
        if(_instance != null && _instance != this){
            //MAking sure there is only one tooltip
            Destroy(this.gameObject);
        }
        else{
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //follow mouse postion
        transform.position = Input.mousePosition;
    }

    public void SetAndShowToolTip(string message){
        gameObject.SetActive(true);
        tooltipText.text = message;
    }

    public void HideTooltip(){
        gameObject.SetActive(false);
        tooltipText.text = string.Empty;
    }
}
