# GX203_TowerDefense_200317

We are tasked to make a tower defense game for my module class Game architecture 

## Pawns Revenge
Pawns Revenge is a tower defense game that is based on the popular board game Chess. Just like in Chess the goal of this game is to protect the king at all costs. The pawns are out for blood!
![Pawns_Revenge_2](/uploads/a73e6a6afa70f257e217c2543d8275d4/Pawns_Revenge_2.gif)

### Features
- Three different towers in the form of the classic chess pieces
- Rook Cannon
- Knight Spear Launcher
- Queen Sorcerer
- Can buy towers to protect the king
- Place Tower pieces directly on the chess board
- Pawns spawn in waves, plus in groups
- Health system for the Pawns and The King
- There is a Shop where you can buy The towers.
- There is only one level. For now.

### How To Play
- Eliminate the Pawns, by buying weaponized chess pieces.
- The more enemies you destroy, the more money you make which means getting more help.
- Place Chess pieces anywhere on the board
- Remember! Towers have Range limits so try to place them where they will protect the king the most.
- Buy Towers, Remember you only have limited money per round.
- Survive a wave of enemies by placing your pieces on the board.
- Watch out, enemies have health and can attack in Large groups.
- You also have health so survive until the wave is over.

### Controls
- Click on the tower chess piesces to select the one you want to place
- Click anywhere on the board, preferbly on the square. 
- This will place your piesce on the board
- Press Escape for the Pause menu
- This will bring you back to menu
- Or you can quit directly from the game

